# Run
> hugo serve

# Add a page

From the site's root run
> hugo new docs/[folder_name]/[file_name]].md

For example to create a page for install:
> hugo new docs/install.md

# Links
Additonal help https://gohugo.io/getting-started/quick-start/
Markdown Cheatsheet https://learn.netlify.com/en/cont/markdown/