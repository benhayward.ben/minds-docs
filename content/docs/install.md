---
title: "Install"
date: 2019-05-05T17:47:11+01:00
weight: 1
---

Minds
=====
Minds is an open-source, encrypted and reward-based social networking platform. https://minds.com

## Repositories

Minds is split into multiple repositories:

- [Engine](https://gitlab.com/Minds/engine) - Backend code & APIs
- [Front](https://gitlab.com/Minds/front) - Client side Angular2 web app
- [Sockets](https://gitlab.com/Minds/sockets) - WebSocket server for real-time communication
- [Mobile](https://gitlab.com/Minds/mobile-native) - React Native mobile apps


## Development System Requirements

- > 10GB RAM (be sure to set it in your docker settings)
- > 100GB Disk space
- [Docker Compose](https://docs.docker.com/compose/)

## Development Installation

**Enabling full installation**

By default, we try not to eat your machine by running the full stack. You'll be able to run the app, but you won't have search and indexing. If you need everthing, be sure to uncomment the ```depends_on``` services in

* runners
* php-fpm

Then you can run:

1. Run `sh init.sh` in order to install the front and engine repositories
2. Run `docker-compose up -d nginx`
3. Run `docker-compose up installer` (one time only.. initial username: minds / password: Pa$$w0rd)
4. Run `docker-compose up front-build` 
5. Navigate to `http://localhost:8080`

### Troubleshooting

- Minds is already installed
  - Ensure engine/settings.php does not exist and re-run `docker-compose up installer`

- Cassandra will not boot
  - Ensure thrift is enabled
  - Cassandra requires at least 4GB of memory to operate. You can start Cassandra manually by running `docker-compose up cassandra`

### Nuclear Option

With dockerized enviroments, it's sometimes best to start from scratch. If you want to delete your data, these steps will completely **delete** your data. You will be starting fresh.

```
  #Remove your settings file
  rm engine/settings.php 
  
  #Stop your stack
  docker-compose down

  #Delete your data cache
  rm -rf .data

  #Purge all volumes
  docker volume prune

  ```

  That will remove all of your locally cached data. You can either rebuild the containers manually by using ```docker-compose up --build``` or delete everything to start fresh.

```
  # Delete all containers
  docker rm $(docker ps -a -q)

```

## Production System Requirements

At this time it is not advisable to run Minds in production, however it is possible so long as you are aware of the risks.

* 3 Cassandra Nodes (Min 30gb RAM, 1TB SSD, 8 CPU)
* 1 ElasticSearch Node (Min 16GB RAM, 250GB SSD, 8 CPU) #2 nodes are recommended for failover
* 1 Docker Machine (Min 60gb RAM, 50GB SSD, 32 CPU)

___
___Copyright Minds 2012 - 2019. 
Copyright for portions of Minds are held by [Elgg](http://elgg.org), 2013 as part of the [Elgg](http://elgg.org) project. All other copyright for Minds is held by Minds, Inc.___