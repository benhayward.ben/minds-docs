---
title: "Contributing"
date: 2019-05-13T17:47:16+01:00

weight: 102
---

## Contributing
If you'd like to help us greatly by contributing to the Minds project head right over to the [Minds Open Source Community](https://www.minds.com/groups/profile/365903183068794880), and reach out to us!

If you've found or fixed a bug, let us know in the [Minds Help and Support Group](https://www.minds.com/groups/profile/100000000000000681/activity)!

___
___Copyright Minds 2012 - 2019. 
Copyright for portions of Minds are held by [Elgg](http://elgg.org), 2013 as part of the [Elgg](http://elgg.org) project. All other copyright for Minds is held by Minds, Inc.___