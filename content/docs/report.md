---
title: "Report an Issue"
date: 2019-05-13T17:47:50+01:00
draft: false
weight: 100
---

## Security reports
Please report all security issues to [security@minds.com](mailto:security@minds.com).

## Reporting a Bug
If your issue does not require a response, you can check our [Report a bug](https://www.minds.com/issues/report) page:

If you require a response, you would be better off posting directly in our Help and Support group.

___
___Copyright Minds 2012 - 2019. 
Copyright for portions of Minds are held by [Elgg](http://elgg.org), 2013 as part of the [Elgg](http://elgg.org) project. All other copyright for Minds is held by Minds, Inc.___