---
title: "Mobile"
date: 2019-05-13T16:35:29+01:00
draft: false
weight: 4
---

## Platforms

- iOS
- Android

## Installing 

Firstly, you are going to need to clone [our repo](https://gitlab.com/minds/mobile-native.git):

``git clone https://gitlab.com/minds/mobile-native.git``

Then, enter the directory, and run:

```yarn install```

## Building
**Android:**

`react-native run-ios`


**iOS:**

`react-native run-android`

## Testing

**Spec tests** can be ran simply with:
`yarn test`

**E2E tests** require that you first set-up [Appium](http://appium.io/).

Following this Appium running, in a separate console, run the following command, substituting the credentials below for that of a "test" user you have set up:

``loginUser=yourtestuser loginPass='youpass' yarn e2e-local __e2e__/``

___
___Copyright Minds 2012 - 2019. 
Copyright for portions of Minds are held by [Elgg](http://elgg.org), 2013 as part of the [Elgg](http://elgg.org) project. All other copyright for Minds is held by Minds, Inc.___