---
title: "Engine"
date: 2019-05-13T16:35:14+01:00
draft: false
weight: 3
---

The back-end system for Minds. Please run inside of [the Minds repo](https://gitlab.com/minds/minds).

## Tasks
To run any CLI jobs, you must run them inside a container:

`docker exec -it minds_php-fpm_1 php '/var/www/Minds/engine/cli.php' controller_name task`

_Note: Help files and parameters are available for some tasks._

## Getting the default admin account

Minds ships with a local user ready to roll:

* **username:** minds
* **password:** Pa$$w0rd

To enable admin functionality, set 'development_mode' to **true** in your settings.php post installation.

## Syncing the newsfeed

1. Make sure you have at least 1 upvote and a hashtag.
2. Assuming your container is named 'minds_php-fpm_1'
3. run ```docker exec -it minds_php-fpm_1 php '/var/www/Minds/engine/cli.php' suggested sync_newsfeed```inside the php-fpm container

## Running php tests

Firstly, you need to have a fully setup development environment so that all the composer dependencies are installed.
To run all tests: 

```bin/phpsec run```

To run a specific spec, include a specific spec file 

```bin/phpspec run Spec/Core/Feeds/Suggested/RepositorySpec.php```

To run a specific test in a spec, include a specific spec file/line number of the test function: 

```bin/phpspec run Spec/Core/Feeds/Suggested/RepositorySpec.php:82```

___
___Copyright Minds 2012 - 2019. 
Copyright for portions of Minds are held by [Elgg](http://elgg.org), 2013 as part of the [Elgg](http://elgg.org) project. All other copyright for Minds is held by Minds, Inc.___