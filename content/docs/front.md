---
title: "Front"
date: 2019-05-13T16:34:48+01:00
draft: false
weight: 2
---

The front-end web application for Minds. Please run inside of [the Minds repo](https://gitlab.com/minds/minds).

### Installation
To install and run the front-end using docker, simply run:

`docker-compose up -d front-build`

*Note: the -d flag detaches the head, so you won't have to keep the console open to use the site. If you are running into errors while developing, you may want to try omiting the -d flag to see the output.*

To install the front-end outside of Docker you'll need to enter the front directory, and run from your console:
```
npm install
npm rebuild node-sass
npm run build-dev
```

### Unit Tests
Run: `npm run test`

### Integration Tests
For integration tests you need to run:

`npm run e2e --config baseUrl=http://localhost --env username=minds,password=Pa$$w0rd`

___
___Copyright Minds 2012 - 2019. 
Copyright for portions of Minds are held by [Elgg](http://elgg.org), 2013 as part of the [Elgg](http://elgg.org) project. All other copyright for Minds is held by Minds, Inc.___